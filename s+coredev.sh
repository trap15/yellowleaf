#!/bin/sh

# Copyright (C) 2007 Segher Boessenkool <segher@kernel.crashing.org>
# Copyright (C) 2009 Hector Martin "marcan" <hector@marcansoft.com>
# Copyright (C) 2009 Andre Heider "dhewg" <dhewg@wiibrew.org>
# Copyright (C) 2010 Alex Marshall "trap15" <SquidMan72@gmail.com>

# Released under the terms of the GNU GPL, version 2

BINUTILS_VER=2.18
BINUTILS_DIR="binutils-$BINUTILS_VER"
BINUTILS_TARBALL="binutils-$BINUTILS_VER.tar.bz2"
BINUTILS_URI="http://ftp.gnu.org/gnu/binutils/$BINUTILS_TARBALL"

GMP_VER=5.0.0
GMP_DIR="gmp-$GMP_VER"
GMP_TARBALL="gmp-$GMP_VER.tar.bz2"
GMP_URI="http://ftp.gnu.org/gnu/gmp/$GMP_TARBALL"

MPFR_VER=2.4.2
MPFR_DIR="mpfr-$MPFR_VER"
MPFR_TARBALL="mpfr-$MPFR_VER.tar.bz2"
MPFR_URI="http://www.mpfr.org/mpfr-current/$MPFR_TARBALL"

GCC_VER=4.4.3
GCC_DIR="gcc-$GCC_VER"
GCC_CORE_TARBALL="gcc-core-$GCC_VER.tar.bz2"
GCC_CORE_URI="http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VER/$GCC_CORE_TARBALL"

BUILDTYPE=$1

TARGET=score-elf

MAKEOPTS=

# End of configuration section.

case `uname -s` in
	*BSD*)
		MAKE=gmake
		;;
	*)
		MAKE=make
esac

export PATH=$SCOREDEV/bin:$PATH

die() {
	echo $@
	exit 1
}

cleansrc() {
	[ -e $SCOREDEV/$BINUTILS_DIR ] && rm -rf $SCOREDEV/$BINUTILS_DIR
	[ -e $SCOREDEV/$GCC_DIR ] && rm -rf $SCOREDEV/$GCC_DIR
}

cleanbuild() {
	[ -e $SCOREDEV/build_binutils ] && rm -rf $SCOREDEV/build_binutils
	[ -e $SCOREDEV/build_gcc ] && rm -rf $SCOREDEV/build_gcc
}

download() {
	DL=1
	if [ -f "$SCOREDEV/$2" ]; then
		echo "Testing $2..."
		tar tjf "$SCOREDEV/$2" >/dev/null && DL=0
	fi

	if [ $DL -eq 1 ]; then
		echo "Downloading $2..."
		wget "$1" -c -O "$SCOREDEV/$2" || die "Could not download $2"
	fi
}

extract() {
	echo "Extracting $1..."
	tar xjf "$SCOREDEV/$1" -C "$2" || die "Error unpacking $1"
}

makedirs() {
	mkdir -p $SCOREDEV/build_binutils || die "Error making binutils build directory $SCOREDEV/build_binutils"
	mkdir -p $SCOREDEV/build_gcc || die "Error making gcc build directory $SCOREDEV/build_gcc"
}

buildbinutils() {
	TARGET=$1
	(
		cd $SCOREDEV/build_binutils && \
		$SCOREDEV/$BINUTILS_DIR/configure --target=$TARGET \
			--prefix=$SCOREDEV --disable-werror --disable-multilib && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building binutils for target $TARGET"
}

buildgcc() {
	TARGET=$1
	(
		cd $SCOREDEV/build_gcc && \
		$SCOREDEV/$GCC_DIR/configure --target=$TARGET --enable-targets=all \
			--prefix=$SCOREDEV --disable-multilib \
			--enable-languages=c --disable-shared --disable-libssp \
			--disable-threads --disable-mudflap --with-newlib && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building GCC for target $TARGET"
}

build() {
	cleanbuild
	makedirs
	echo "******* Building SCORE-ELF binutils"
	buildbinutils $TARGET
	echo "******* Building SCORE-ELF GCC"
	buildgcc $TARGET
	echo "******* SCORE-ELF toolchain built and installed"
}

if [ -z "$SCOREDEV" ]; then
	die "Please set SCOREDEV in your environment."
fi

case $BUILDTYPE in
	score|clean)	;;
	"")
		die "Please specify build type (score/clean)"
		;;
	*)
		die "Unknown build type $BUILDTYPE"
		;;
esac

if [ "$BUILDTYPE" = "clean" ]; then
	cleanbuild
	cleansrc
	exit 0
fi

download "$BINUTILS_URI" "$BINUTILS_TARBALL"
download "$GMP_URI" "$GMP_TARBALL"
download "$MPFR_URI" "$MPFR_TARBALL"
download "$GCC_CORE_URI" "$GCC_CORE_TARBALL"

cleansrc

extract "$BINUTILS_TARBALL" "$SCOREDEV"
extract "$GCC_CORE_TARBALL" "$SCOREDEV"
extract "$GMP_TARBALL" "$SCOREDEV/$GCC_DIR"
mv "$SCOREDEV/$GCC_DIR/$GMP_DIR" "$SCOREDEV/$GCC_DIR/gmp" || die "Error renaming $GMP_DIR -> gmp"
extract "$MPFR_TARBALL" "$SCOREDEV/$GCC_DIR"
mv "$SCOREDEV/$GCC_DIR/$MPFR_DIR" "$SCOREDEV/$GCC_DIR/mpfr" || die "Error renaming $MPFR_DIR -> mpfr"

case $BUILDTYPE in
	score)		build ;;
esac
