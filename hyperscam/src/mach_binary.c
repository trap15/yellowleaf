#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "mach_types.h"
#include "memory.h"
#include "emulator.h"

#include "mach_memory.h"

u32 openRaw(core7Cpu* cpu, char *path, u32 addr)
{
	u8* buf = NULL;
	u32 sz;
	FILE* fd = fopen(path, "rb");
	if(fd == NULL) {
		return -1;
	}
	fseek(fd, 0, SEEK_END);
	sz = ftell(fd);
	fseek(fd, 0, SEEK_SET);
	buf = malloc(sz);
	fread(buf, 1, sz, fd);
	fclose(fd);
	printf("Size    :         0x%08X\n", sz);
	printf("Address :         0x%08X\n", addr);
	memcopy_load(cpu, buf, addr, sz);
	free(buf);
	return addr;
}
