#include "types.h"
#include "mach_types.h"

irq_t hw_irqs[] = { \
	{ 0, 0, 63, "SPU FIQ" }, \
	{ 0, 1, 62, "SPU Beat IRQ" }, \
	{ 0, 2, 61, "SPU Env IRQ" }, \
	{ 0, 3, 60, "CD Servo" }, \
	{ 0, 4, 59, "ADC Gain Overflow / ADC Recorder / FIFO Overflow" }, \
	{ 0, 5, 58, "General Purpose ADC" }, \
	{ 0, 6, 57, "Timer Base" }, \
	{ 0, 7, 56, "Timer" }, \
	{ 1, 8, 55, "TV VBlank Start" }, \
	{ 1, 9, 54, "LCD VBlank Start" }, \
	{ 1,10, 53, "PPU VBlank Start" }, \
	{ 1,11, 52, "TV (?)" }, \
	{ 1,12, 51, "Sensor Frame End" }, \
	{ 1,13, 50, "Sensor Coordinate Hit" }, \
	{ 1,14, 49, "Sensor Motion Frame End" }, \
	{ 1,15, 48, "Sensor Capture Done / Sensor Debug IRQ" }, \
	{ 2,16, 47, "TV Coordinate Hit" }, \
	{ 2,17, 46, "PPU Coordinate Hit" }, \
	{ 2,18, 45, "USB Host+Device" }, \
	{ 2,19, 44, "SIO" }, \
	{ 2,20, 43, "SPI" }, \
	{ 2,21, 42, "UART (IrDA)" }, \
	{ 2,22, 41, "NAND" }, \
	{ 2,23, 40, "SD" }, \
	{ 3,24, 39, "I2C Master" }, \
	{ 3,25, 38, "I2S Slave" }, \
	{ 3,26, 37, "APBDMA Ch1" }, \
	{ 3,27, 36, "APBDMA Ch2" }, \
	{ 3,28, 35, "LDM DMA" }, \
	{ 3,29, 34, "BLN DMA" }, \
	{ 3,30, 33, "APBDMA Ch3" }, \
	{ 3,31, 32, "APBDMA Ch4" }, \
	{ 4,32, 31, "Alarm+HMS" }, \
	{ 4,33, 30, "MP4" }, \
	{ 4,34, 29, "C3 (ECC module)" }, \
	{ 4,35, 28, "GPIO" }, \
	{ 4,36, 27, "Bufctl (Debug) + TV/PPU VBlank End (Debug)" }, \
	{ 4,37, 26, "RESERVED 1" }, \
	{ 4,38, 25, "RESERVED 2" }, \
	{ 4,39, 24, "RESERVED 3" }, \
};

