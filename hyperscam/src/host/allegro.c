#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "mach_types.h"
#include <allegro.h>

#include "host/host.h"

int host_current_page;
BITMAP* host_video_page[2];
BITMAP* host_video_buffer;
int screen_bpp;
sprite_t sprites[512];
bg_t bgs[3];
int screen_size;
u32 host_palette[PALCOUNT][256];
