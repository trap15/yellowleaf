#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "mach_types.h"
#include <allegro.h>

#include "host/host.h"

int screen_x = 320;
int screen_y = 240;
int hoffset[512];
core7MappedMemory* gfxbufbanks[13];
u32 gfxbufaddrs[13];
int spr_widths[] = { 8, 16, 32, 64 };
int spr_heights[] = { 8, 16, 32, 64 };
