#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "types.h"
#include "mach_types.h"
#include "emulator.h"
#include "memory.h"

#include "host_config.h"
#include "host/host.h"
#include "mach_binary.h"
#include "mach_emulator.h"
#include "mach_memory.h"
#include "mach_hw_core.h"
#include "mach_hw_update.h"
#if (HOST_TYPE == HOST_ALLEGRO)
/* We only need this for the END_OF_MAIN() thingy, ok :| */
#include <allegro.h>
#endif

#ifdef DEBUG
#define TICK_AT_A_TIME		1
#endif

char ascii(char s)
{
	if(s < 0x20) return '.';
	if(s > 0x7E) return '.';
	return s;
}

void __hexdump(void *d, int len) 
{
	u8 *data;
	int i, off;
	data = (u8*)d;
	for(off = 0; off < len; off += 16) {
		printf("%08X ", off);
		for(i = 0; i < 16; i++) {
			if((i + off) >= len)
				printf("   ");
			else
				printf("%02X ", data[off + i]);
		}
		printf(" | ");
		for(i = 0; i < 16; i++) {
			if((i + off) >= len)
				printf(" ");
			else
				printf("%c", ascii(data[off + i]));
		}
		printf("\n");
	}
}

void bitdump(void *data, int sz)
{
	sz = sz << 2;
	printf("Sz %i\n", sz);
	while (sz--) {
		printf("%X ", (((u8 *)*(&data))[0] >> sz) & 0x1);
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	char* filename;
	char* firmware;
	s64 ret;
	core7Cpu* cpu;
	
	printf("HyperScam\n");
	printf("A Mattel HyperScan emulator by trap15 using yellowLeaf\n");

	if(argc < 2) {	/* No arguments passed. */
		filename = calloc(256, 1);
		sprintf(filename, "HYPER.EXE");
	}else{
		filename = calloc(strlen(argv[1]) + 1, 1);
		sprintf(filename, "%s", argv[1]);
	}
	if(argc < 3) {	/* Firmware not specified. */
		firmware = calloc(256, 1);
		sprintf(firmware, "FW.BIN");
	}else{
		firmware = calloc(strlen(argv[2]) + 1, 1);
		sprintf(firmware, "%s", argv[2]);
	}
	printf("Initializing host code\n");
	ret = host_initialize();
	if(ret < 0) {
		printf("Error initializing host! Cleaning up and exiting!\n");
		free(firmware);
		free(filename);
		exit(1);
	}
#ifdef DEBUG
	printf("Debug mode enabled\n");
#endif
	printf("Initializing the CPU core...\n");
	cpu = initializeCPU(0xA0FFFFFC);
	
	printf("Mapping the RAM...\n");
	ret = mapMemory(cpu, 0xA0000000, 0x01000000, FLAG_RAM);
	printf("Memory map was%s successful\n", ret ? "" : " NOT");
	if(ret < 0) {
		printf("Failed.\n");
		exit(1);
	}
	printf("Mapping RAM mirror...\n");
	ret = mirrorMemory(cpu, 0x80000000, 0x01000000, 0xA0000000, FLAG_RAM);
	printf("Memory mirror was%s successful\n", ret ? "" : " NOT");
	if(ret < 0) {
		printf("Failed.\n");
		exit(1);
	}
	printf("Mapping the NOR flash...\n");
	ret = mapMemory(cpu, 0x9F000000, 0x01000000, FLAG_ROM);
	printf("Flash map was%s successful\n", ret ? "" : " NOT");
	if(ret < 0) {
		printf("Failed.\n");
		exit(1);
	}
	printf("Mapping NOR flash mirror...\n");
	ret = mirrorMemory(cpu, 0x9E000000, 0x01000000, 0x9F000000, FLAG_ROM);
	printf("Flash mirror was%s successful\n", ret ? "" : " NOT");
	if(ret < 0) {
		printf("Failed.\n");
		exit(1);
	}
	
	printf("Loading Firmware...\n");
	ret = openRaw(cpu, firmware, 0x9F000000);
	if((u32)ret == (u32)-1) {
		printf("Error loading %s.\n", filename);
		unmapMemory(cpu);
		free(filename);
		exit(1);
	}
	printf("Loaded.\n");
	printf("Loading Binary...\n");
	ret = openRaw(cpu, filename, 0xA00901FC);
	if((u32)ret == (u32)-1) {
		printf("Error loading %s.\n", filename);
		unmapMemory(cpu);
		free(filename);
		exit(1);
	}
	printf("Loaded.\n");
	
	setPC(cpu, 0x9F000000);
	
	for(;;) {
#if TICK_AT_A_TIME
//		fgetc(stdin);
#endif
		HW_UpdateBefore(cpu);
#if DEBUG
		executeOpcode(cpu, readOpcode(cpu, getPC(cpu)));
//		printRegisters(cpu);
		printf("PC: 0x%08X\n", cpu->pc);
#else
		runProcessor(cpu, CLOCK_SPEED / SCREEN_FPS);
#endif
		HW_UpdateAfter(cpu);
	}
	
	printf("Execution finished... unmapping the ram\n");
	unmapMemory(cpu);
	free(filename);
	
	return 0;
}

#if (HOST_TYPE == HOST_ALLEGRO)
END_OF_MAIN()
#endif
