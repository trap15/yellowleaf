#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "types.h"
#include "mach_types.h"
#include "memory.h"
#include "emulator.h"
#include "instructions.h"
#include "dasm.h"

#include "mach_memory.h"
#include "mach_hw_core.h"

s8 mmio_mem[MMIO_SIZE];

/* 
 * Initialize the cpu emulation core. 
 */

core7Cpu* initializeCPU(u32 stackPtr)
{
	core7Cpu* cpu = calloc(sizeof(core7Cpu), 1);
	cpu->r[0] = stackPtr;
	return cpu;
}
