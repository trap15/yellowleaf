#ifndef HOST_CONFIG_H
#define HOST_CONFIG_H

#define HOST_UNKNOWN	0
#define HOST_ALLEGRO	1
#define HOST_SDL	2

#define HOST_TYPE	(HOST_ALLEGRO)

#endif
