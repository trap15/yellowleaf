#ifndef MACH_MEMORY_H
#define MACH_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include "memory.h"
#include "types.h"
#include "mach_types.h"
#include "emulator.h"

#include "mach_hw_core.h"
#include "mach_hw.h"

INLINE s8 readByte(core7Cpu* cpu, u32 address)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 1)) {
		return handleMMIOReadByte(cpu, address);
	}
	address = transformAddr(cpu, address, 1, &bank, 1);
	return (bank->memory[address]);
}

INLINE s16 readHword(core7Cpu* cpu, u32 address)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 2)) {
		return handleMMIOReadHword(cpu, address);
	}
	address = transformAddr(cpu, address, 2, &bank, 1);
#if (((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE)))
	return *((u16*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 1] << ( 8 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 0] << ( 0 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) ;
#endif
}

INLINE s32 readWord(core7Cpu* cpu, u32 address)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 4)) {
		return handleMMIOReadWord(cpu, address);
	}
	address = transformAddr(cpu, address, 4, &bank, 1);
#if (((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE)))
	return *((u32*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 3] << (24 - (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 2] << (16 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 1] << ( 8 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 0] << ( 0 + (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))) ;
#endif
}

INLINE u32 readOpcode(core7Cpu* cpu, u32 address)
{
	core7MappedMemory* bank;
	address = transformAddr(cpu, address, 4, &bank, 1);
#if (((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE)))
	return *((u32*)(&(bank->memory[address])));
#else
	return	(bank->memory[address + 3] << (24 - (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 2] << (16 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 1] << ( 8 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) |
		(bank->memory[address + 0] << ( 0 + (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))) ;
#endif
}

INLINE void writeByte(core7Cpu* cpu, u32 address, u8 value)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 1)) {
		handleMMIOWriteByte(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 1, &bank, 2);
	bank->memory[address] = value;
}

INLINE void writeHword(core7Cpu* cpu, u32 address, u16 value)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 2)) {
		handleMMIOWriteHword(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 2, &bank, 2);
#if ((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE))
	*((u16*)(&(bank->memory[address]))) = value;
#else
	bank->memory[address + 1] = (u8)(value >> ( 8 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE))));
	bank->memory[address + 0] = (u8)(value >> ( 0 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE))));
#endif
}

INLINE void writeWord(core7Cpu* cpu, u32 address, u32 value)
{
	core7MappedMemory* bank;
	if(memWithinRange(MMIO_START, MMIO_SIZE, address, 4)) {
		handleMMIOWriteWord(cpu, address, value);
		return;
	}
	address = transformAddr(cpu, address, 4, &bank, 2);
#if ((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE))
	*((u32*)(&(bank->memory[address]))) = value;
#else
	bank->memory[address + 3] = (u8)(value >> (24 - (24 * (TARGET_ENDIAN == ENDIANNESS_BE))));
	bank->memory[address + 2] = (u8)(value >> (16 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE))));
	bank->memory[address + 1] = (u8)(value >> ( 8 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE))));
	bank->memory[address + 0] = (u8)(value >> ( 0 + (24 * (TARGET_ENDIAN == ENDIANNESS_BE))));
#endif
}

/*
 * Memory functions
 */

INLINE void memcopy(core7Cpu* cpu, void *src, u32 address, int size)
{
	u32 x;
	u8* s = src;
	core7MappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 2);
	for (x = 0; x < size; x++, s++)
		bank->memory[address + x] = *s;
}

INLINE void memcopy_load(core7Cpu* cpu, void *src, u32 address, int size)
{
	u32 x;
	u8* s = src;
	core7MappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 0);
	for (x = 0; x < size; x++, s++)
		bank->memory[address + x] = *s;
}

INLINE void memoryset(core7Cpu* cpu, u32 address, u8 fill, int size)
{
	int x;
	core7MappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 2);
	for (x = 0; x < size; x++)
		bank->memory[address + x] = fill;
}

INLINE void memoryset_load(core7Cpu* cpu, u32 address, u8 fill, int size)
{
	int x;
	core7MappedMemory* bank;
	address = transformAddr(cpu, address, size, &bank, 0);
	for (x = 0; x < size; x++)
		bank->memory[address + x] = fill;
}



#endif
