#ifndef MACH_EMULATOR_H
#define MACH_EMULATOR_H

#include "types.h"
#include "mach_types.h"
#include "emulator.h"

core7Cpu* initializeCPU(u32 stackPtr);

#endif
