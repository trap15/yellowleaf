#ifndef HOST_H
#define HOST_H

#include "types.h"
#include "../mach_types.h"
#include "../host_config.h"

#define PALCOUNT	256

typedef enum {
	GFXMODE_IDX_4	= 0,
	GFXMODE_IDX_16,
	GFXMODE_IDX_64,
	GFXMODE_IDX_256,
	GFXMODE_BITMAP_ARGB,
	GFXMODE_BITMAP_RGB565,
} gfxmode_t;

typedef struct {
	gfxmode_t	mode;
	int		number;		/* Index into the sprite buffer array */
	int		pal;
	int		flip;		/* bit1 = vert, bit0 = hori */
	int		layer;
	int		x, y, w, h;
	void*		pixels;
	void*		host_version;
	int		host_flip;
	int		needs_update;
	int		needs_flip;
} sprite_t;

typedef struct {
	int		enable;
	int		mode_type;
	gfxmode_t	mode;
	int		pal;
	int		flip;		/* bit1 = vert, bit0 = hori */
	int		layer;
	int		x, y, w, h;
	int		hoff_en;
	int		vcmp_en;
	int		hcmp_en;
	int		chnum_ptr;
	void*		pixels;
	void*		host_version;
	int		host_flip;
	int		needs_update;
	int		needs_flip;
} bg_t;

extern core7MappedMemory* gfxbufbanks[];
extern u32 gfxbufaddrs[];
extern int spr_widths[];
extern int spr_heights[];
extern int hoffset[];
extern int screen_x;
extern int screen_y;

#if (HOST_TYPE == HOST_SDL)
#include "host_sdl.h"
#elif (HOST_TYPE == HOST_ALLEGRO)
#include "host_allegro.h"
#else
#error Could not determine which host code to use!
#endif

#endif
