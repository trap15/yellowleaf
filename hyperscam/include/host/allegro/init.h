#ifndef HOST_INIT_H
#define HOST_INIT_H

#include "types.h"
#include "mach_types.h"
#include <allegro.h>

#include "../../mach_config.h"
#include "../host.h"
#include "video.h"

INLINE int host_initialize()
{
	int ret;
	printf("Host is: ALLEGRO\nInitializing...\n");
	ret = allegro_init();
	if(ret != 0) {
		printf("Main ALLEGRO init failed on %d in %s!\n", __LINE__, __FILE__);
		return -1;
	}
	ret = install_keyboard();
	if(ret != 0) {
		printf("Main ALLEGRO init failed on %d in %s!\n", __LINE__, __FILE__);
		return -1;
	}
	ret = install_timer();
	if(ret != 0) {
		printf("Main ALLEGRO init failed on %d in %s!\n", __LINE__, __FILE__);
		return -1;
	}
	printf("Main ALLEGRO init succeeded!\nGraphics initializing...\n");
	
	screen_bpp = 32;		/* TODO: Choose a good color depth */
	set_color_depth(screen_bpp);
	ret = set_gfx_mode(GFX_AUTODETECT, screen_x, screen_y, 0, 0);
	if(ret != 0) {
		set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
		printf("Graphics init failed on %d in %s!\n", __LINE__, __FILE__);
	}
	host_video_page[0] = create_video_bitmap(SCREEN_W, SCREEN_H);
	host_video_page[1] = create_video_bitmap(SCREEN_W, SCREEN_H);
	host_current_page = 0;	
	
	printf("Graphics init succeeded!\nHost initialization complete!\n");
	
	return 0;
}

#endif
