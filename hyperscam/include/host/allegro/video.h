#ifndef HOST_VIDEO_H
#define HOST_VIDEO_H

#include "types.h"
#include "mach_types.h"
#include <allegro.h>

#include "../../mach_config.h"
#include "../host.h"

extern int host_current_page;
extern BITMAP* host_video_page[];
extern BITMAP* host_video_buffer;
extern int screen_bpp;
extern sprite_t sprites[512];
extern bg_t bgs[3];
extern int screen_size;
extern u32 host_palette[PALCOUNT][256];

INLINE void draw_screen(BITMAP* src)
{
	blit(src, screen, 0, 0, 0, 0, src->w, src->h);
}

INLINE void _video_convert_pixels(void* pxl, int pal, gfxmode_t mode, int w, int h, unsigned char **line, int hoff[])
{
	int x, y;
	u32 pix;
	int transp;
	u8 idx;
	for(y = 0; y < h; y++) {
		for(x = ((hoff != NULL) ? hoff[y] : 0); x < w; x++) {
			transp = 0;
			switch(mode) {
				case GFXMODE_IDX_4:
					idx = (((u8*)pxl)[(x + (y * w)) >> 2] >> (2 * ((x + (y * w)) % 4))) & 0x3;
					break;
				case GFXMODE_IDX_16:
					idx = (((u8*)pxl)[(x + (y * w)) >> 1] >> (4 * ((x + (y * w)) % 2))) & 0xF;
					break;
				case GFXMODE_IDX_64:
					/* TODO: Do this. This mode eats babies, btw. */
					break;
				case GFXMODE_IDX_256:
					idx = ((u8*)pxl)[(x + (y * w))];
					break;
				default:
					break;
			}
			switch(mode) {
				case GFXMODE_IDX_4:
				case GFXMODE_IDX_16:
				case GFXMODE_IDX_64:
				case GFXMODE_IDX_256:
					pix = host_palette[pal][idx];
					if(pix == 0xFFFFFFFF)
						transp = 1;
					break;
				
				case GFXMODE_BITMAP_ARGB:
					if((((u16*)pxl)[(x + (y * w))] >> 15) & 0x1) {
						transp = 1;
					}else{
						pix =	(((((u16*)pxl)[(x + (y * w))] >> 10) & 0x1F) << (16+3)) |	\
							(((((u16*)pxl)[(x + (y * w))] >>  5) & 0x1F) << ( 8+3)) |	\
							(((((u16*)pxl)[(x + (y * w))] >>  0) & 0x1F) << ( 0+3));
					}
					break;
				case GFXMODE_BITMAP_RGB565:
					pix =	(((((u16*)pxl)[(x + (y * w))] >> 11) & 0x1F) << (16+3)) | \
						(((((u16*)pxl)[(x + (y * w))] >>  5) & 0x3F) << ( 8+2)) | \
						(((((u16*)pxl)[(x + (y * w))] >>  0) & 0x1F) << ( 0+3));
					break;
			}
			switch(screen_bpp) {
				case 32:
					if(transp)
						pix = MASK_COLOR_32;
					((u32*)(line[y]))[x] = pix;
					break;
				case 24:
					if(transp)
						pix = MASK_COLOR_24;
					((u32*)(line[y]))[x] = pix;
					break;
				case 16:
					if(transp)
						pix = MASK_COLOR_16;
					((u16*)(line[y]))[x] =	(((pix >> ( 0+3)) & 0x1F) <<  0) | \
								(((pix >> ( 8+2)) & 0x3F) <<  5) | \
								(((pix >> (16+3)) & 0x1F) << 11);
					break;
				case 15:
					if(transp)
						pix = MASK_COLOR_15;
					((u16*)(line[y]))[x] =	(((pix >> ( 0+3)) & 0x1F) <<   0) | \
								(((pix >> ( 8+3)) & 0x1F) <<   5) | \
								(((pix >> (16+3)) & 0x1F) <<  10);
					break;
			}
		}
	}
}

INLINE void _video_build_host_sprite(sprite_t* spr)
{
	BITMAP* bmp = create_bitmap(spr->w, spr->h);
	int size = 0;
	switch(spr->mode) {
		case GFXMODE_IDX_4:
			size = spr->w * spr->h / 64;
			break;
		case GFXMODE_IDX_16:
			size = spr->w * spr->h / 16;
			break;
		case GFXMODE_IDX_64:
			size = spr->w * spr->h / 4;
			break;
		case GFXMODE_IDX_256:
			size = spr->w * spr->h;
			break;
		case GFXMODE_BITMAP_RGB565:
		case GFXMODE_BITMAP_ARGB:
			size = spr->w * spr->h * 2;
			break;
		default:
			break;
	}
	if(spr->pixels == NULL) {
		spr->pixels = malloc(size);
	}
	memcpy(spr->pixels, &(gfxbufbanks[12]->memory[gfxbufaddrs[12]]), size);
	_video_convert_pixels(spr->pixels, spr->pal, spr->mode, spr->w, spr->h, bmp->line, NULL);
	spr->host_version = bmp;
}

INLINE void _video_build_host_bg(bg_t* bg)
{
	BITMAP* bmp = create_bitmap(bg->w, bg->h);
//	_video_convert_pixels(bg->pixels, bg->pal, bg->mode, bg->w, bg->h, bmp->line, bg->hoffset);
	bg->host_version = bmp;
}

INLINE void video_blit_sprite(sprite_t* spr)
{
	BITMAP* bmp;
	if(spr->host_version != NULL) {
		free(spr->host_version);
	}
	_video_build_host_sprite(spr);
	switch(spr->flip) {
		case 0:
			spr->host_flip = DRAW_SPRITE_NO_FLIP;
			break;
		case 1:
			spr->host_flip = DRAW_SPRITE_H_FLIP;
			break;
		case 2:
			spr->host_flip = DRAW_SPRITE_V_FLIP;
			break;
		case 3:
			spr->host_flip = DRAW_SPRITE_VH_FLIP;
			break;
	}
	bmp = spr->host_version;
	draw_sprite_ex(host_video_buffer, bmp, spr->x, spr->y, DRAW_SPRITE_NORMAL, spr->host_flip);
}

INLINE void video_blit_bg(bg_t* bg)
{
	BITMAP* bmp;
	if(bg->host_version != NULL) {
		free(bg->host_version);
	}
	_video_build_host_bg(bg);
	switch(bg->flip) {
		case 0:
			bg->host_flip = DRAW_SPRITE_NO_FLIP;
			break;
		case 1:
			bg->host_flip = DRAW_SPRITE_H_FLIP;
			break;
		case 2:
			bg->host_flip = DRAW_SPRITE_V_FLIP;
			break;
		case 3:
			bg->host_flip = DRAW_SPRITE_VH_FLIP;
			break;
	}
	bmp = bg->host_version;
	draw_sprite_ex(host_video_buffer, bmp, 0, 0, DRAW_SPRITE_NORMAL, bg->host_flip);
}

INLINE void video_update()
{
	sprite_t *sprs[4][512];
	bg_t *bgups[4];
	int sprcnts[4];
	int i, l;
	
	for(i = 0; i < 4; i++) {
		for(l = 0; l < 512; l++) {
			sprs[i][l] = NULL;
		}
		bgups[i] = NULL;
		sprcnts[i] = 0;
	}
	for(i = 0; i < 512; i++) {
		sprs[sprites[i].layer][sprcnts[sprites[i].layer]++] = &sprites[i];
	}
	for(i = 0; i < 3; i++) {
		if(bgs[i].enable)
			bgups[bgs[i].layer] = &bgs[i];
	}
	for(i = 0; i < 8; i++) {
		if(i % 2) {
			for(l = 0; l < sprcnts[(i - 1) >> 1]; l++) {
				if(sprs[(i - 1) >> 1][l] != NULL) {
					video_blit_sprite(sprs[(i - 1) >> 1][l]);
				}
			}
		}else{
			if(bgups[i >> 1] != NULL) {
//				video_blit_bg(bgups[i >> 1]);
			}
		}
	}
	
	blit(host_video_buffer, host_video_page[host_current_page], 0, 0, 0, 0, SCREEN_W, SCREEN_H);
	draw_screen(host_video_page[host_current_page]);
	show_video_bitmap(host_video_page[host_current_page]);
	host_current_page = host_current_page ^ 1;
}

#endif
