#ifndef MACH_CONFIG_H
#define MACH_CONFIG_H

#include "types.h"
#include "mach_types.h"

#define CLOCK_SPEED		(162 * 1000 * 1000)
#define SCREEN_FPS		(30)
#define TARGET_ENDIAN		ENDIANNESS_LE

extern int screen_x;
extern int screen_y;

#endif
