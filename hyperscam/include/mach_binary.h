#ifndef _BINARY_H
#define _BINARY_H

#include "types.h"
#include "mach_types.h"
#include "emulator.h"

u32 openRaw(core7Cpu* cpu, char *path, u32 addr);

#endif
