#ifndef MACH_HW_UPDATE_H
#define MACH_HW_UPDATE_H

#include "types.h"
#include "mach_types.h"
#include "emulator.h"

#include "mach_memory.h"
#include "mach_hw_core.h"
#include "mach_hw.h"

INLINE void HW_UpdateBefore(core7Cpu* cpu)
{
	setRegister(cpu, REGISTER_DREG, 0x20000000);	/* Kludge to pass another part of the firmware */
}

INLINE void HW_UpdateAfter(core7Cpu* cpu)
{
	
}

#endif
