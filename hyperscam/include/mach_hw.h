#ifndef MACH_HW_H
#define MACH_HW_H

#include "types.h"
#include "mach_types.h"
#include "emulator.h"

#include "mach_hw_core.h"
#include "host/host.h"

#define MMIO_WRITE8(a, d)	\
	mmio_mem[(a) - 0x88000000] = (d)
#define MMIO_READ8(a)	\
	(mmio_mem[(a) - 0x88000000])
#if ((_BE_) && (TARGET_ENDIAN == ENDIANNESS_BE)) || ((_LE_) && (TARGET_ENDIAN == ENDIANNESS_LE))
#	define MMIO_WRITE16(a, d)	\
		*((u16*)(&(mmio_mem[(a) - 0x88000000]))) = (d)
#	define MMIO_WRITE32(a, d)	\
		*((u32*)(&(mmio_mem[(a) - 0x88000000]))) = (d)
#	define MMIO_READ16(a)	\
		(*((u16*)(&(mmio_mem[(a) - 0x88000000]))))
#	define MMIO_READ32(a)	\
		(*((u32*)(&(mmio_mem[(a) - 0x88000000]))))

#else
#	define MMIO_WRITE16(a, d)	\
		mmio_mem[(a) - 0x88000000 + 1] = (u8)((d) >> (8 - (8 * (TARGET_ENDIAN == ENDIANNESS_BE)))); \
		mmio_mem[(a) - 0x88000000 + 0] = (u8)((d) >> (0 + (8 * (TARGET_ENDIAN == ENDIANNESS_BE))))
#	define MMIO_WRITE32(a, d)	\
		mmio_mem[(a) - 0x88000000 + 3] = (u8)((d) >> (24 - (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))); \
		mmio_mem[(a) - 0x88000000 + 2] = (u8)((d) >> (16 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))); \
		mmio_mem[(a) - 0x88000000 + 1] = (u8)((d) >> ( 8 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))); \
		mmio_mem[(a) - 0x88000000 + 0] = (u8)((d) >> ( 0 + (24 * (TARGET_ENDIAN == ENDIANNESS_BE))))
#	define MMIO_READ16(a)	\
		((mmio_mem[(a) - 0x88000000 + 1] << ( 8 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) | \
		 (mmio_mem[(a) - 0x88000000 + 0] << ( 0 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))))
#	define MMIO_READ32(a)	\
		((mmio_mem[(a) - 0x88000000 + 3] << (24 - (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))) | \
		 (mmio_mem[(a) - 0x88000000 + 2] << (16 - ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) | \
		 (mmio_mem[(a) - 0x88000000 + 1] << ( 8 + ( 8 * (TARGET_ENDIAN == ENDIANNESS_BE)))) | \
		 (mmio_mem[(a) - 0x88000000 + 0] << ( 0 + (24 * (TARGET_ENDIAN == ENDIANNESS_BE)))))
#endif

extern irq_t hw_irqs[];
extern s8 mmio_mem[MMIO_SIZE];

INLINE void MMIOInit()
{
	bzero(mmio_mem, MMIO_SIZE);
	MMIO_WRITE32(MMIO_SDRAM_STATUS, 1);
}

INLINE s8 handleMMIOReadByte(core7Cpu* cpu, u32 address)
{
	
#if DEBUG
	printf("MMIO Read Byte from %08X\n", address);
#endif
	return MMIO_READ8(address);
}

INLINE s16 handleMMIOReadHword(core7Cpu* cpu, u32 address)
{
	
#if DEBUG
	printf("MMIO Read Hword from %08X\n", address);
#endif
	return MMIO_READ16(address);
}

INLINE s32 handleMMIOReadWord(core7Cpu* cpu, u32 address)
{
	
#if DEBUG
	printf("MMIO Read Word from %08X\n", address);
#endif
	return MMIO_READ32(address);
}

INLINE void handleMMIOWriteByte(core7Cpu* cpu, u32 address, u8 data)
{
	if((address >= MMIO_PPU_SPR_CHARNUM(0)) && (address < MMIO_PPU_SPR_CHARNUM(512))) {
		int spr = (address - MMIO_PPU_SPR_CHARNUM(0)) % 8;
		switch(address - MMIO_PPU_SPR_CHARNUM(spr) + MMIO_PPU_SPR_CHARNUM(0)) {
			case MMIO_PPU_SPR_CHARNUM(0) + 1:
				sprites[spr].number &= ~0xFF00;
				sprites[spr].number |= data << 8;
				break;
			case MMIO_PPU_SPR_CHARNUM(0) + 0:
				sprites[spr].number &= ~0x00FF;
				sprites[spr].number |= data;
				break;
			case MMIO_PPU_SPR_X(0) + 1:
				sprites[spr].x &= ~0xFF00;
				sprites[spr].x |= data << 8;
				break;
			case MMIO_PPU_SPR_X(0) + 0:
				sprites[spr].x &= ~0x00FF;
				sprites[spr].x |= data;
				break;
			case MMIO_PPU_SPR_ATTR(0) + 1:
				sprites[spr].pal = ((data >> 8) & 0x1F);
				sprites[spr].layer = ((data >> 13) & 0x03);
				((data >> 15) & 0x01); /* Blend */
				break;
			case MMIO_PPU_SPR_ATTR(0) + 0:
				sprites[spr].mode = ((data >> 0) & 0x03);
				sprites[spr].flip = ((data >> 2) & 0x03);
				sprites[spr].w = spr_widths[((data >> 4) & 0x03)];
				sprites[spr].h = spr_heights[((data >> 6) & 0x03)];			
				break;
			case MMIO_PPU_SPR_Y(0) + 1:
				sprites[spr].y &= ~0xFF00;
				sprites[spr].y |= data << 8;
				break;
			case MMIO_PPU_SPR_Y(0) + 0:
				sprites[spr].y &= ~0x00FF;
				sprites[spr].y |= data;
				break;
		}
	}
#if DEBUG
	printf("MMIO Write Byte %02X from %08X\n", data, address);
#endif
	MMIO_WRITE8(address, data);
}

INLINE void handleMMIOWriteHword(core7Cpu* cpu, u32 address, u16 data)
{
	if((address >= MMIO_PPU_SPR_CHARNUM(0)) && (address < MMIO_PPU_SPR_CHARNUM(512))) {
		int spr = (address - MMIO_PPU_SPR_CHARNUM(0)) % 8;
		switch(address - MMIO_PPU_SPR_CHARNUM(spr) + MMIO_PPU_SPR_CHARNUM(0)) {
			case MMIO_PPU_SPR_CHARNUM(0):
				sprites[spr].number = data;
				break;
			case MMIO_PPU_SPR_X(0):
				sprites[spr].x = data;
				break;
			case MMIO_PPU_SPR_ATTR(0):
				sprites[spr].flip = ((data >> 2) & 0x03);
				sprites[spr].w = spr_widths[((data >> 4) & 0x03)];
				sprites[spr].h = spr_heights[((data >> 6) & 0x03)];
				sprites[spr].pal = ((data >> 8) & 0x1F);
				sprites[spr].layer = ((data >> 13) & 0x03);
				sprites[spr].mode = ((data >> 0) & 0x03);
				((data >> 15) & 0x01); /* Blend */
				break;
			case MMIO_PPU_SPR_Y(0):
				sprites[spr].y = data;
				break;
		}
	}
#if DEBUG
	printf("MMIO Write Hword %04X from %08X\n", data, address);
#endif
	MMIO_WRITE16(address, data);
}

INLINE void handleMMIOWriteWord(core7Cpu* cpu, u32 address, u32 data)
{
	if((address >= MMIO_PPU_SPR_CHARNUM(0)) && (address < MMIO_PPU_SPR_CHARNUM(512))) {
		int spr = (address - MMIO_PPU_SPR_CHARNUM(0)) % 8;
		switch(address - MMIO_PPU_SPR_CHARNUM(spr) + MMIO_PPU_SPR_CHARNUM(0)) {
			case MMIO_PPU_SPR_CHARNUM(0):
				sprites[spr].number = data & 0x0000FFFF;
				sprites[spr].x = data & 0xFFFF0000;
				break;
			case MMIO_PPU_SPR_ATTR(0):
				sprites[spr].flip = ((data >> 2) & 0x03);
				sprites[spr].w = spr_widths[((data >> 4) & 0x03)];
				sprites[spr].h = spr_heights[((data >> 6) & 0x03)];
				sprites[spr].pal = ((data >> 8) & 0x1F);
				sprites[spr].layer = ((data >> 13) & 0x03);
				sprites[spr].mode = ((data >> 0) & 0x03);
				((data >> 15) & 0x01); /* Blend */
				sprites[spr].y = data & 0xFFFF0000;
				break;
		}
	}else
#if 0
	if((address >= MMIO_PPU_BG_X(0)) && (address < MMIO_PPU_BG_X(3))) {
		int bg = (address - MMIO_PPU_BG_X(0)) % 0x1C;
		switch(address) {
			case MMIO_PPU_BG_X(bg):
				bgs[bg].x = data;
				break;
			case MMIO_PPU_BG_Y(bg):
				bgs[bg].y = data;
				break;
			case MMIO_PPU_BG_ATTR(bg):
				bgs[bg].pal = ((data >> 8) & 0x1F);
				bgs[bg].layer = ((data >> 13) & 0x03);
				bgs[bg].flip = ((data >> 2) & 0x1F);
				bgs[bg].w = ((data >> 4) & 0x03);
				bgs[bg].h = ((data >> 6) & 0x03);
				break;
			case MMIO_PPU_BG_CTRL(bg):
				bgs[bg].mode_type = ((data >> 0) & 0x01);
				bgs[bg].enabled = ((data >> 3) & 0x01);
				bgs[bg].hoff_en = ((data >> 4) & 0x01);
				if(bg == 0)
					bgs[bg].hcmp_en = ((data >> 5) & 0x01);
				bgs[bg].vcmp_en = ((data >> 6) & 0x01);
				((data >> 8) & 0x01);	/* Blend */
				break;
			case MMIO_PPU_BG_CHNM_PTR(bg):
				bgs[bg].chnum_ptr = data;
				break;
			case MMIO_PPU_BG_BLEND(bg):
				break;
		}
	}else
#endif
	{
		int idx = 0;
		switch(address) {
			case MMIO_PPU_IMG_BUF(4, 0):
				idx++;
			case MMIO_PPU_IMG_BUF(3, 2):
				idx++;
			case MMIO_PPU_IMG_BUF(3, 1):
				idx++;
			case MMIO_PPU_IMG_BUF(3, 0):
				idx++;
			case MMIO_PPU_IMG_BUF(2, 2):
				idx++;
			case MMIO_PPU_IMG_BUF(2, 1):
				idx++;
			case MMIO_PPU_IMG_BUF(2, 0):
				idx++;
			case MMIO_PPU_IMG_BUF(1, 2):
				idx++;
			case MMIO_PPU_IMG_BUF(1, 1):
				idx++;
			case MMIO_PPU_IMG_BUF(1, 0):
				idx++;
			case MMIO_PPU_IMG_BUF(0, 2):
				idx++;
			case MMIO_PPU_IMG_BUF(0, 1):
				idx++;
			case MMIO_PPU_IMG_BUF(0, 0):
				gfxbufaddrs[idx] = transformAddr(cpu, data, 1, &(gfxbufbanks[idx]), 1);
				break;
		}
	}
#if DEBUG
	printf("MMIO Write Word %08X from %08X\n", data, address);
#endif
	MMIO_WRITE32(address, data);
}

#endif
