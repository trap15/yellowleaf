#ifndef MACH_TYPES_H
#define MACH_TYPES_H

#include "types.h"

typedef struct {
	int	slave;
	int	source;
	int	vector;
	char	string[128];
} irq_t;

#endif
