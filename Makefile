all:
	make -C hyperscam all
clean:
	make -C hyperscam clean
run:
	make -C hyperscam run
release:
	make -C hyperscam release
