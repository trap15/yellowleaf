#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "emulator.h"
#include "dasm.h"
#include "instructions.h"

char *registerName[69] = {
	" r00", " r01", " r02", " r03", " r04", " r05", " r06", " r07",
	" r08", " r09", " r10", " r11", " r12", " r13", " r14", " r15",
	" r16", " r17", " r18", " r19", " r20", " r21", " r22", " r23",
	" r24", " r25", " r26", " r27", " r28", " r29", " r30", " r31",
	"  HI", "  LO", "sr00", "sr01", "sr02", "cr00", "cr01", "cr02",
	"cr03", "cr04", "cr05", "cr06", "cr07", "cr08", "cr09", "cr10",
	"cr11", "cr12", "cr13", "cr14", "cr15", "cr16", "cr17", "cr18",
	"cr19", "cr20", "cr21", "cr22", "cr23", "cr24", "cr25", "cr26",
	"cr27", "cr28", "cr29", "cr30", "cr31",
};

char* registerToName(core7Register reg)
{
	return registerName[reg];
}

char *dasmFormat(char *haystack, core7Dasm *dasm)
{
	char formattedStr[128];
	
	char *hptr = haystack;
	char *fptr = formattedStr;
	
	char fmtBuf[16];
	
	*fptr = '\0';
	
	/* TODO: This */
	(void)(fmtBuf);
	(void)(hptr);
	
	return (char *)strdup(formattedStr);
}					


