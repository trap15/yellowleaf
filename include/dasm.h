#ifndef _DASM_H
#define _DASM_H

#include "types.h"

char* dasmFormat(char *haystack, core7Dasm *dasm);
char* registerToName(core7Register reg);

extern char *registerName[];
extern core7InstrTbl *instructionTables[];
extern int instructionShifts[];
extern int instructionAnds[];

INLINE void dasmOpcodeBig(core7Cpu* cpu, u32 opcode, core7Dasm *ret)
{
	core7Dasm *dasm = ret;
	
	dasm->table = INSTRUCTION_32(opcode);
	dasm->instruction = (opcode >> instructionShifts[dasm->table]) & instructionAnds[dasm->table];
	dasm->data = opcode;
	
	if(instructionTables[dasm->table] == NULL) {
#if DEBUG
		printf("Table %d for 32bit not supported.\n", dasm->table);
#endif
		return;
	}
	dasm->delay = instructionTables[dasm->table][dasm->instruction].delay;
}

INLINE void dasmOpcodeSmall(core7Cpu* cpu, u16 opcode, core7Dasm *ret)
{
	core7Dasm *dasm = ret;
	
	dasm->table = INSTRUCTION_16(opcode) + 32;
	dasm->instruction = (opcode >> instructionShifts[dasm->table]) & instructionAnds[dasm->table];
	dasm->data = opcode;
	
	if(instructionTables[dasm->table] == NULL) {
#if DEBUG
		printf("Table %d for 16bit not supported.\n", dasm->table);
#endif
		return;
	}
	dasm->delay = instructionTables[dasm->table][dasm->instruction].delay;
}

INLINE int getOpcodeSize(u32 opcode)
{
	if((opcode & (1 << 31)) && (opcode & (1 << 15)))
		return 1;				/* 32 bit */
	return 0;					/* 16 bit */
}

INLINE u32 getRealOpcode(u32 opcode)
{
	return ((opcode & 0x7FFF) | ((opcode & ~(0xFFFF)) >> 1));
}

#endif
