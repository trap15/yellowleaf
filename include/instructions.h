#ifndef _INSTRUCTIONS_H
#define _INSTRUCTIONS_H

#include "types.h"
#include "dasm.h"

extern int _branched;

extern core7InstrTbl *instructionTables[];

INLINE core7Instruction execGenericOpcode(core7Cpu* cpu, void *d)
{
	core7Instruction ret = 0;
	core7Dasm *dasm = d;
	if(instructionTables[dasm->table] == NULL) {
#if DEBUG
		printf("Unhandled table (%d, %d)\n", dasm->table, dasm->instruction);
#endif
		return ret;
	}
	if(instructionTables[dasm->table][dasm->instruction].execute == NULL) {
#if DEBUG
		printf("Unhandled instruction (%d, %d)\n", dasm->table, dasm->instruction);
#endif
		return ret;
	}
	ret = instructionTables[dasm->table][dasm->instruction].execute(cpu, dasm);
	advanceCycles(cpu, instructionTables[dasm->table][dasm->instruction].cycles);
	return ret;
}

INLINE core7Instruction execBigOpcode(core7Cpu* cpu, void *d)
{
	_branched = 0;
	core7Instruction ret = execGenericOpcode(cpu, d);
	if(!_branched)
		advancePC(cpu, 4);
	return ret;
}

INLINE char* textBigOpcode(core7Cpu* cpu, void *d)
{
	/* TODO: Fill this out */
	return "TODO";
}

INLINE core7Instruction execSmallOpcode(core7Cpu* cpu, void *d, void *e)
{
	_branched = 0;
	core7Instruction ret = execGenericOpcode(cpu, d);
	if(!_branched) {
		advancePC(cpu, 2);
	}else if((getPC(cpu) % 4) == 2) {
		advancePC(cpu, -2);
	}
	return ret;
}

INLINE char* textSmallOpcode(core7Cpu* cpu, void *d)
{
	/* TODO: Fill this out */
	return "TODO";
}

#endif
