#ifndef _EMULATOR_H
#define _EMULATOR_H

#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memory.h"

extern struct _core7Dasm opc;
extern struct _core7Dasm opc2;
extern int _branched;

char* registerToName(core7Register reg);

INLINE void setFlag(core7Cpu* cpu, int flag, int set)
{
	if(set) {
		cpu->r[REGISTER_COND] |=  flag;
	}else{
		cpu->r[REGISTER_COND] &= ~flag;
	}
}

INLINE int getFlag(core7Cpu* cpu, int flag)
{
	return ((cpu->r[REGISTER_COND] & flag) ? 1 : 0);
}

INLINE void setRegister(core7Cpu* cpu, core7Reg reg, core7Register value)
{
	cpu->r[reg] = value;
}

INLINE core7Register readRegister(core7Cpu* cpu, core7Reg reg)
{
	return cpu->r[reg];
}

INLINE void advanceCycles(core7Cpu* cpu, int cycles)
{
	cpu->cycles += cycles;
}

INLINE void advancePC(core7Cpu* cpu, core7Register nextPC)
{
	cpu->pc += nextPC;
}

INLINE void setPC(core7Cpu* cpu, core7Register pc)
{
	cpu->pc = pc;
}

INLINE core7Register getPC(core7Cpu* cpu)
{
	return cpu->pc;
}

INLINE void printRegisters(core7Cpu* cpu)
{
	int reg;
	
	for(reg = 0; reg < 69; reg++) {
		printf("%s: 0x%08X", registerToName(reg), cpu->r[reg]);
		if((reg % 4) == 3)
			printf("\n");
		else
			printf(" ");
	}
	printf("PC: 0x%08X\n", cpu->pc);
}

INLINE void generateExceptionCore(core7Cpu* cpu, u32 exception, u32 vec)
{
	/* TODO: Verify this */
	setRegister(cpu, REGISTER_EPC, getPC(cpu));			/* Set the exception PC */
	setRegister(cpu, REGISTER_ECR, exception);			/* Set the exception cause register */
	/* Save the IE and KU bits */
	cpu->savedPSR = readRegister(cpu, REGISTER_PSR) & 0x00000001; /* TODO: KU bit saving */
	/* Save the N, Z, C, V and T bits */
	cpu->savedCCR = readRegister(cpu, REGISTER_CCR) & (CPUFLAG_N | CPUFLAG_Z | CPUFLAG_C | CPUFLAG_V | CPUFLAG_T);
	setPC(cpu, vec - 4);	/* We have to subtract 4, since it would increase the PC after this */
}

INLINE void generateExceptionBase(core7Cpu* cpu, u32 exception, u32 spoff)
{
	core7Register vec = readRegister(cpu, REGISTER_EXCPVEC);	/* Get the exception vector */
	generateExceptionCore(cpu, exception, vec + 0x1FC + spoff);
}

INLINE void generateException(core7Cpu* cpu, u32 exception, u32 param)
{
	/* TODO: Get/verify the offsets for the various exceptions */
	core7Register off = 0;
	switch(exception) {
		case EXCEPTION_BREAKPOINT:
			break;
		case EXCEPTION_SYSCALL:
		case EXCEPTION_TRAP:
			off += 4;
			break;
		default:
			off += 4;
			break;
	}
	generateExceptionBase(cpu, ((exception & 0x3F) << 18) | param, off);
}

INLINE void generateIRQ(core7Cpu* cpu, u32 irq)
{
	generateExceptionBase(cpu, (irq & 0x3F) << 18, 4 + (irq * 4));	/* IRQs start at 1 */
}

INLINE int memWithinRange(u32 addr1, u32 size1, u32 addr2, u32 size2)
{
	if(addr1 <= addr2)
		if((addr1 + size1) > (addr2 + size2))
			return 1;
	return 0;
}

INLINE u32 transformAddr(core7Cpu* cpu, u32 address, int size, core7MappedMemory **bank, int type)
{
	*bank = getBank(cpu, address, size, type);
	if(*bank == NULL) {
		printf("EXCEPTION : %s(%#x)\n", __FUNCTION__, address);
		if(type == 0)
			type = 2;
		/* TODO: Generate an exception */
		exit(1);
	}
	int addr = (address - (*bank)->addrStart);
	return addr;
}

#include "instructions.h"
#include "dasm.h"
#include "mach_memory.h"

INLINE void executeOpcode(core7Cpu* cpu, u32 opcode)
{
#ifdef DEBUG
	printf("Executing opcode: %08X\n", opcode);
#endif
	switch(getOpcodeSize(opcode)) {
		case 1:
			opcode = getRealOpcode(opcode);
			dasmOpcodeBig(cpu, opcode, &opc);
			execBigOpcode(cpu, &opc);
			break;
		case 0:
			dasmOpcodeSmall(cpu, opcode & 0xFFFF, &opc);
			execSmallOpcode(cpu, &opc, &opc2);
			break;
	}
}

INLINE void runProcessor(core7Cpu* cpu, int cycles)
{
	if(cycles == 0) cycles = 1;
	for(cpu->cycles = 0; cpu->cycles < cycles; )
		executeOpcode(cpu, readOpcode(cpu, getPC(cpu)));
}

#endif
