#ifndef _TYPES
#define _TYPES

#include <sys/param.h>
#include <stdint.h>
#include "mach_config.h"

/* Defines */

#ifdef __APPLE__
#ifndef __MACOSX__
#define __MACOSX__	1
#endif
#endif

#ifdef __BYTE_ORDER
#	if __BYTE_ORDER == __LITTLE_ENDIAN
#		define _LE_	1
#		define _BE_	0
#	elif __BYTE_ORDER == __BIG_ENDIAN
#		define _LE_	0
#		define _BE_	1
#	else
#		error :Unknown host byte order!
#	endif
#endif /* __BYTE_ORDER */

#ifndef CLEAN
#define INLINE			__attribute__((always_inline)) inline
#else
#define INLINE			static
#endif

#define FLAG_ROM		(0x1)
#define FLAG_RAM		(0x2)
#define FLAG_MIRROR		(0x3)

#define ENDIANNESS_BE		(0x02)
#define ENDIANNESS_LE		(0x01)

#define REGISTER_HI 		(32)
#define REGISTER_LO		(33)
#define REGISTER_SR(x) 		(34 + (x))
#define REGISTER_CNT 		REGISTER_SR(0)
#define REGISTER_LCR 		REGISTER_SR(1)
#define REGISTER_SCR 		REGISTER_SR(2)


/* Control Registers:
	|3|3|2|2|2|2|2|2|2|2|2|2|1|1|1|1|1|1|1|1|1|1|0|0|0|0|0|0|0|0|0|0|
	|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|9|8|7|6|5|4|3|2|1|0|
 
 cr00:	Processor Status Register (PSR)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|I|
	I: Interrupt Enable
 
 cr01:	Condition Register
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr02:	Exception Cause Register (ECR)
	|?|?|?|?|?|?|?|?|I|I|I|I|I|I|?|?|?|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|
	I: Interrupt Vector (cause)
	P: Parameter (for syscall and trap)
 
 cr03:	Exception Vector Register (EXCPvec)
	|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|V|
	V: Exception Vector
 
 cr04:	Core Control Register (CCR)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|L|?|M|?|
	L: LDM Enable
	M: MMU Enable
 
 cr05:	Exception Program Counter (EPC)
	|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|
	P: PC before exception occurred
 
 cr06:	Exception Memory Address (EMA)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr15:	LIM Physical Page Number Register (LIMPFN)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr16:	LDM Physical Page Number Register (LDMPFN)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr18:	Processor Revision Register (Prev)
	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr29:	Debug Control Register (DREG)
 	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 cr30:	Debug Exception Program Counter (DEPC)
	|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|P|
	P: PC before debug exception occurred
 
 cr31:	Debug Save Register (DSAVE)
 	|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|?|
 
 */
#define REGISTER_CR(x) 		(37 + (x))
#define REGISTER_PSR		REGISTER_CR(0)
#define REGISTER_COND		REGISTER_CR(1)
#define REGISTER_ECR		REGISTER_CR(2)
#define REGISTER_EXCPVEC	REGISTER_CR(3)
#define REGISTER_CCR		REGISTER_CR(4)
#define REGISTER_EPC		REGISTER_CR(5)
#define REGISTER_EMA		REGISTER_CR(6)
#define REGISTER_LIMPFN		REGISTER_CR(15)
#define REGISTER_LDMPFN		REGISTER_CR(16)
#define REGISTER_P_REV		REGISTER_CR(18)
#define REGISTER_DREG		REGISTER_CR(29)
#define REGISTER_DEPC		REGISTER_CR(30)
#define REGISTER_DSAVE		REGISTER_CR(31)

#define INSTRUCTION_32(opcode)	((opcode >> 25) & 31)
#define INSTRUCTION_16(opcode)	((opcode >> 12) &  7)

/* TODO: These are not the correct ones, but the SunPlus doc doesn't say what
 *       they should be, so I just improvised some. Should work fine though. */
#define CPUFLAG_N		(1 << 0)
#define CPUFLAG_Z		(1 << 1)
#define CPUFLAG_C		(1 << 2)
#define CPUFLAG_V		(1 << 3)
#define CPUFLAG_T		(1 << 4)

/* Typedefs */

typedef uint8_t			u8;
typedef uint16_t		u16;
typedef uint32_t		u32;
typedef uint64_t		u64;
typedef int8_t			s8;
typedef int16_t			s16;
typedef int32_t			s32;
typedef int64_t			s64;

typedef s32			core7Register_s;
typedef u32			core7Register_u;
typedef core7Register_s		core7Register;

typedef u8			core7Reg;

typedef u32			core7JumpAddress_u;
typedef s32			core7JumpAddress_s;
typedef core7JumpAddress_u	core7JumpAddress;

typedef s16			core7SignedImmediate;
typedef u16			core7UnsignedImmediate;

typedef u32			core7Instruction;

/* Enumerations */

typedef enum {
	EXCEPTION_SYSCALL	= 7,
	EXCEPTION_BREAKPOINT	= 9,	/* TODO: Get actual exception number */
	EXCEPTION_TRAP		= 10,
} exception_t;

/* Structures */

typedef struct _core7Dasm {
	u8	delay;
	u8	size;
	u8	instruction;
	u8	table;
	u32	data;
} core7Dasm;

typedef struct _core7MappedMemory {
	u32 addrStart;
	u32 addrEnd;
	u32 size;
	u8  flags;
	u8* memory;
	struct _core7MappedMemory *next;
} core7MappedMemory;

typedef struct _core7Cpu {
	/* Program counters. */
	core7Register pc;
	/* Cycle counter */
	int cycles;
	/* Registers */
	core7Register r[69];
	/* Exception stuff */
	core7Register savedPSR;
	core7Register savedCCR;
	/* Debugging stuff */
	int inDebug;
	int iceEnabled;
	int sjProbeEnabled;
	
	struct _core7MappedMemory *rootBank;
	struct _core7MappedMemory *mappedBanks;
} core7Cpu;

typedef struct _core7InstrTbl {
	core7Instruction (*execute)(struct _core7Cpu* cpu, core7Dasm *dasm);
	char textDisasm[20];
	u32 mask;
	u32 value;
	u8 delay;
	u8 cycles;
} core7InstrTbl;

#endif
